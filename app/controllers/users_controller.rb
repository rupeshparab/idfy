require 'json'
require 'open-uri'

class UsersController < ApplicationController

  before_action :hits, :status
  def hits
    source = "https://idfy.0x10.info/api/idfy-status?type=json&query=api_hits"
    return JSON.parse(JSON.load(open(source)).to_json)["api_hits"]
  end

  def status
    return User.where.not(status: nil).count
  end

  def ethn str
    if str == "0"
      return "Asian"
    elsif str == "1"
      return "Indian"
    elsif str == "2"
      return "African Americans"
    elsif str == "3"
      return "Asian Americans"
    elsif str == "4"
      return "European"
    elsif str == "5"
      return "British"
    elsif str == "6"
      return "Jewish"
    elsif str == "7"
      return "Latino"
    elsif str == "8"
      return "Native American"
    elsif str == "9"
      return "Arabic"
    else
      return nil
    end
  end

  def index
    @users = User.paginate(:page => params[:page], :per_page => 5)
    @hits = hits
    @status = status
  end

  def show
    @users = User.where(ethnicity: ethn(params[:id])).paginate(:page => params[:page], :per_page => 5)
    @hits = hits
    @status = status
    render :index
  end
end
