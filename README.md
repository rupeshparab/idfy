== README - IDFy

I have modified the asset pipeline to include fonts folder, besides that everything
is normal.
I have seeded data using the JSON api provided

The instructions to run this project are:

Migrate the table and seed data in the table, in code I have seeded till page 200

I have completed all the basic features and I completed it within 7 hours
Most of my time was spent on designing. I have used a drag and drop design creator
to create the UI.

Some of the features like favorite, download, share have been only shown in the UI,
but it's backend code is not complete.

I have included screenshots in the zip.

Regards,
Rupesh Parab
