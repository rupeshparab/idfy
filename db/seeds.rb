# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

require 'json'
require 'open-uri'

def to_b num
  if num == "1"
    return true
  end
  return false
end


def ethn num
  if num == "0"
    return "Asian"
  elsif num == "1"
    return "Indian"
  elsif num == "2"
    return "African Americans"
  elsif num == "3"
    return "Asian Americans"
  elsif num == "4"
    return "European"
  elsif num == "5"
    return "British"
  elsif num == "6"
    return "Jewish"
  elsif num == "7"
    return "Latino"
  elsif num == "8"
    return "Native American"
  elsif num == "9"
    return "Arabic"
  else
    return null
  end
end


User.delete_all

(1..200).each do |i|
    source = "https://idfy.0x10.info/api/idfy-status?type=json&query=list_member&page="+i.to_s
    #puts data
    JSON.parse(JSON.load(open(source)).to_json)["members"].each do |data|
      User.create(status: data["status"], ethnicity: ethn(data["ethnicity"]),
                  weight: data["weight"].to_f/1000, height: data["height"].to_i,
                  is_veg: to_b(data["is_veg"]), drink: to_b(data["drink"]),
                  dob: data["dob"].to_date, image: data["image"]
      )
    end
end
