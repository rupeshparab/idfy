class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.text    :status, :null => true
      t.string  :ethnicity, :null => true
      t.float   :weight, :null => true
      t.integer :height, :null => true
      t.boolean :is_veg, :null => true
      t.boolean :drink, :null => true
      t.boolean :favourite, :default => false
      t.date    :dob, :null => true
      t.string  :image, :null => true
      t.timestamps  :null => true
    end
  end
end
